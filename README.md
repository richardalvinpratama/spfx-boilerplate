# SPFx-Boilerplate

## BoilerPlate Code Documentation

Berikut penjelasan struktur dan code boilerplate Sharepoint Framework

- [BoilerPlate Code Documentation](./src)

## Set up Microsoft 365 Tenant

- Buat akun Microsoft dan login
- Cari SharePoint App (jika tidak ada, harus upgrade akun microsoft)
- Pilih create site -> Team Site atau Communication Site

## Set up Development Environment

- Pakai IDE favorit kamu (repo ini menggunakan Visual Studio Code)
- Install Node (versi node dapat dicheck pada file package.json --> engines)
- Install beberapa tool atau package untuk development menggunakan command di bawah

```
npm install gulp-cli yo @microsoft/generator-sharepoint --global
```

- konfigurasikan development environment kita untuk mempercayai sertifikat Self-signed SSL

```
gulp trust-dev-cert
```

## Getting Started Web Parts

- buat webpart baru

```
yo @microsoft/sharepoint
```

- update serve.json --> initialPage dengan mengganti "enter-your-sharepoint-site" ke url site microsoft 365 tenant yang telah kita buat
- gulp serve (nanti akan terbuka webpage yaitu workbench untuk kita debug webpart yang dibuat)
- pastikan muncul "reload" atau "\_runwatch" setelah itu refresh workbench agar kode yang diubah dapat berfungsi
- disini kita bisa memasukkan webparts default yang telah dibuat oleh "yo @microsoft/sharepoint"
- kita juga bisa mengedit webparts default sesuai dengan keinginan kita pada direktori src/webparts/

## Connect Webpart to SharePoint

Disini kita bisa mengambil data baik list, file, dll pada sharepoint kita untuk ditampilkan pada webparts custom kita

- Ini adalah SharePoint REST APIs yang bisa digunakan

```
https://yourtenantprefix.sharepoint.com/_api/web/lists
```

## Deploy WebParts Custom kita ke SharePoint Page

Webparts custom yang kita buat, belum dapat digunakan pada SharePoint Page. Oleh karena itu kita perlu deploy dengan langkah dibawah ini:

- package webparts custom kita
- pastikan url sharepoint sudah sesuai dengan sharepoint tenant yang ingin dideploy
- update version (bisa increment satu versionnya misal 1.0.1 menjadi 1.0.2) di config/package-solution.json --> version
- jalankan command dibawah untuk membuat package dengan format .sppkg di folder sharepoint/solution

```
gulp bundle --ship
gulp package-solution --ship
```

- Deploy the webparts custom package to app catalog (buka sharepoint admin center --> more --> open apps)
- upload or drag and drop the webpart-custom.sppkg file
- Install the client-side solution on your site
- setelah itu kita bisa membuka kembali site sharepoint kita dan mencoba menambahkan webparts custom yang telah dideploy

## How to integrate microsoft graph api into sharepoint framework

You can see the code in src/webparts/graphApi

## Version Issue When Build or Run

If you got error when try to build or run sharepoint framework in your local, it's because the package is outdated
for example: you clone project A, and make new webpart custom. If the yo microsoft update a new version and suddenly you can't run or build.
you must check the package.json
This is the step to check and updated the outdated package

- "npm outdated" in your terminal
- this will show the outdated package inside your package.json
- try to update the package based on the error that you get when build or run the sharepoint framework
- for ex: npm install @microsoft/sp-build-web@1.18.2

## References

- https://learn.microsoft.com/en-us/sharepoint/dev/spfx/sharepoint-framework-overview

import { Log } from '@microsoft/sp-core-library';
import {
  BaseApplicationCustomizer, PlaceholderContent, PlaceholderName
} from '@microsoft/sp-application-base';

import styles from "./NavbarApplicationCustomizer.module.scss";
import * as strings from 'NavbarApplicationCustomizerStrings';
import { frontend_URL } from '../../helpers/UrlHelper';

const LOG_SOURCE: string = 'NavbarApplicationCustomizer';

/**
 * If your command set uses the ClientSideComponentProperties JSON input,
 * it will be deserialized into the BaseExtension.properties object.
 * You can define an interface to describe it.
 */
export interface INavbarApplicationCustomizerProperties {
  // This is an example; replace with your own property
  Top: string;
  Bottom: string;
}

/** A Custom Action which can be run during execution of a Client Side Application */
export default class NavbarApplicationCustomizer
  extends BaseApplicationCustomizer<INavbarApplicationCustomizerProperties> {
  
  private _topPlaceholder: PlaceholderContent | undefined; //for Navbar
  private _bottomPlaceholder: PlaceholderContent | undefined; //for Footer

  private _onDispose():void{
    console.log("Navbar and Footer Disposed");
  }

  public async onInit(): Promise<void> {
    Log.info(LOG_SOURCE, `Initialized ${strings.Title}`);

    const headerGlobal = document.querySelector(
      `#spSiteHeader`
    ) as HTMLElement | null;

    const searchGlobal = document.querySelector(`#sbcId`) as HTMLElement | null;
    headerGlobal?.classList.add(styles.visuallyhidden);
    searchGlobal?.classList.add(styles.visuallyhidden);

    this.context.placeholderProvider.changedEvent.add(
      this,
      this._render
    )

    return Promise.resolve();
  }

  private _render(): void{
    //Handling the Navbar Placeholder
    if(!this._topPlaceholder){
      this._topPlaceholder = this.context.placeholderProvider.tryCreateContent(
        PlaceholderName.Top,
        { onDispose: this._onDispose}
      )

      if(this._topPlaceholder?.domElement){
        const menuItems = [
          {
            text: "About Us",
            text_id: "Tentang Kami",
            dataMenu: "aboutUs",
            link: `${frontend_URL}/SitePages/About-Us.aspx`
          },
          {
            text: "Announcement",
            text_id: "Pengumuman",
            dataMenu: "announcement",
            link: `${frontend_URL}/SitePages/Announcement.aspx`
          },
          {
            text: "Seminar",
            text_id: "Seminar",
            dataMenu: "seminar",
            link: `${frontend_URL}/SitePages/Seminar.aspx`
          },
          {
            text: "Benefit",
            text_id: "Keuntungan",
            dataMenu: "benefit",
            link: `${frontend_URL}/SitePages/Benefit.aspx`
          },
          {
            text: "Policy",
            text_id: "kebijakan",
            dataMenu: "policy",
            link: `${frontend_URL}/SitePages/Policy.aspx`
          }
        ]

        const linkMenu = menuItems
            .map(
              (item, index) => `
              <li><a href="${item.link}">${item.text}</a></li>
            `
            )
            .join("");

        const navbarUp = `
            <div class="${styles.navbarUp}">
              <div class="${styles.navbarUp__left}">
                <div style="margin-right: 25px;">
                  <a href="${frontend_URL}" class="${styles.logo}">
                    <img src="${require("../../assets/icon/suitmedia_logo_white.png")}"/>
                  </a>
                </div>
                <div>
                  <a href="https://suitmedia.com" target="_blank" class="${
                    styles.navbarUp__menu__left
                  }">Suitmedia Digital Agency</a>
                </div>
              </div>
              <div class="${styles.links}">
                  <ul>
                    ${linkMenu}
                  </ul>
              </div>
            </div>
            `;
            
        this._topPlaceholder.domElement.innerHTML = `
          <div class="${styles.navbar}">
            ${navbarUp}
          </div>
        `;
      }
    }

    //Handling the Footer Placeholder
    if(!this._bottomPlaceholder){
      this._bottomPlaceholder = this.context.placeholderProvider.tryCreateContent(
        PlaceholderName.Bottom,
        { onDispose: this._onDispose }
      )

      if(this._bottomPlaceholder?.domElement){
        this._bottomPlaceholder.domElement.innerHTML = `
          <div class="${styles.app}">
            <div class="${styles.bottom}">
              <p>Copyright © 2023 Suitmedia Digital Agency, All right Reserved</p>
            </div>
          </div>
        `;
      }
    }
  }
}

declare interface INavbarApplicationCustomizerStrings {
  Title: string;
}

declare module 'NavbarApplicationCustomizerStrings' {
  const strings: INavbarApplicationCustomizerStrings;
  export = strings;
}

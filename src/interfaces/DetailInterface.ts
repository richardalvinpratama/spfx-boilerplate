export interface Details {
    value: Detail[];
}

export interface Detail{
    Title: string;
    Image: string;
    Body: string;
    Created: string;
    Author: PersonCreatedDetail;
}
  
export interface PersonCreatedDetail{
    Title: string;
}
  
  
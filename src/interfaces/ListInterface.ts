export interface Lists {
    value: List[];
}

export interface List{
    Title: string;
    ThumbnailImage: string;
    Created: string;
    Url_Map: string;
}
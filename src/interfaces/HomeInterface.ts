export interface HomeLists{
    value: HomeList[];
}

export interface HomeList{
    ID: string;
    Title: string;
    SubTitle: string;
    ThumbnailImage: string;
    Created:string;
}
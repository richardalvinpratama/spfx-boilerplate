# BoilerPlate Code Documentation

Berikut merupakan list folder dan file yang perlu diperhatikan dalam pengembahan kode sharepoint framework boilerplate

- src folder
  - tempat kita membuat custom webpart, konfigurasi API, helper, extensions, dan lainnya
- config folder
  - tempat kita konfigurasi workbench url, version, permission, dan webpart entry point

## Webparts Folder

- ketika kita membuat webpart baru menggunakan "yo @microsoft/sharepoint", webpart baru akan terbuat didalam folder ini
- Untuk pembuatan struktur webpart dapat dilakukan di CustomWebPart.ts sedangkan tampilan dapat di CustomWebPart.module.scss
- juga terdapat folder component (pembuatan card, button, filter, sort, dan lainnya yang dapat dipakai di webpart custom)

## Interfaces Folder

## API Folder

## Helper Folder

## Extensions Folder

## Assets Folder

import { Version } from '@microsoft/sp-core-library';
import {
  type IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import { SPHttpClient, SPHttpClientResponse } from "@microsoft/sp-http";
import styles from './HomeWebPart.module.scss';
import { HomeLists, HomeList } from "../../interfaces/HomeInterface";
import { cmsAPI_URL } from "../../helpers/UrlHelper";

export interface IHomeWebPartProps {
  about_title_1: string;
  about_desc_1: string;
  about_title_2: string;
  about_desc_2: string;
  about_title_3: string;
  about_desc_3: string;
}

export default class HomeWebPart extends BaseClientSideWebPart<IHomeWebPartProps> {
  private Data: HomeList[];

  public async render(): Promise<void> {
    await this._renderListAsync();

    const aboutTab = `
      <div class="${styles.card}">
        <img src="${require("../../assets/img/lamp.png")}">
        <h1>${escape(this.properties.about_title_1)}</h1>
        <p>${this.properties.about_desc_1}</p>
      </div>
      <div class="${styles.card}">
        <img src="${require("../../assets/img/rocket.png")}">
        <h1>${escape(this.properties.about_title_2)}</h1>
        <p>${this.properties.about_desc_2}</p>
      </div>
      <div class="${styles.card}">
        <img src="${require("../../assets/img/sattelite.png")}">
        <h1>${escape(this.properties.about_title_3)}</h1>
        <p>${this.properties.about_desc_3}</p>
      </div>
    `;

    const infoTab = `
      <div class="${styles.infoTab}"> 
        <div class="${styles.cardTab}">
          <h1>Announcement</h1>
        </div>
        <div class="${styles.cardTab}">
          <h1>Seminar</h1>
        </div>
        <div class="${styles.cardTab}">
          <h1>Benefit</h1>
        </div>
        <div class="${styles.cardTab}">
          <h1>Policy</h1>
        </div>
      </div>
    `;

    this.domElement.innerHTML = `
    <section class="${styles.home}">
      <div class="${styles.about}">
        ${aboutTab}
      </div>
      <div class="${styles.info}">
        ${infoTab}
        <div class="${styles.infoData}">
          <div id="data-information" class="${styles.card_wrapper}">
          </div>
        </div>
      </div>
    </section>
    `;

    this.renderDataHTML();
  }

  protected onInit(): Promise<void> {
    return super.onInit();
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  private async _getListData(): Promise<HomeLists> {
		return this.context.spHttpClient
			.get(
				`${cmsAPI_URL}/GetByTitle('Announcement')/Items?$orderby=Created desc&$top=8`,
				SPHttpClient.configurations.v1
			)
			.then((response: SPHttpClientResponse) => {
				return response.json();
			});
	}

  private async _renderListAsync(): Promise<void> {
		try {
			const response = await this._getListData();
			this.Data = response.value;
		} catch (error) {
			alert(`Failed Get Data`);
		}
	}

  private renderDataHTML() : void {
    const data = this.Data.map((data: HomeList) => {
			const date = new Date(data.Created);
			const formattedDate = `${date.getDate()} ${date.getMonth()} ${date.getFullYear()}`;

      return `
        <div class="${styles.card}">
          <div class="${styles.image_container}">
            <svg xmlns="http://www.w3.org/2000/svg" width="68" height="68" viewBox="0 0 68 68" fill="none">
              <g clip-path="url(#clip0_596_752)">
              <path d="M49.988 0H24V8.04H28V4H48V20H64V52H47.932V56H68V16.728L49.988 0ZM52 7.328L61.34 16H52V7.328ZM0 12V68H44V28.728L25.988 12H0ZM28 19.328L37.34 28H28V19.328ZM4 64V16H24V32H40V64H4Z" fill="#02453D"/>
              </g>
              <defs>
              <clipPath id="clip0_596_752">
                <rect width="68" height="68" fill="white"/>
              </clipPath>
              </defs>
            </svg>
          </div>
          <div class="${styles.container}">
            <div class="${styles.upper}">
                <h3><b>${data.Title}</b></h3>  
                <p>${data.SubTitle}</p>
                <p>Created: ${formattedDate}</p>
            </div>
            <div class="${styles.lower}">
                <button>Detail</button>
            </div>
          </div>
        </div>
      `;
    })
    
    const dataHTML = this.domElement.querySelector("#data-information");
		if (dataHTML !== null) {
			dataHTML.innerHTML = this.Data.length ? data.join("") : "";
		}

  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: "Dynamic Home Input"
          },
          groups: [
            {
              groupName: "About_Home_1",
              groupFields: [
                PropertyPaneTextField('about_title_1', {
                  label: "About_Title_1"
                }),
                PropertyPaneTextField('about_desc_1', {
                  label: "About-Desc-1"
                })
              ]
            },
            {
              groupName: "About_Home_2",
              groupFields: [
                PropertyPaneTextField('about_title_2', {
                  label: "About_Title_2"
                }),
                PropertyPaneTextField('about_desc_2', {
                  label: "About_Desc_2"
                })
              ]
            },
            {
              groupName: "About_Home_3",
              groupFields: [
                PropertyPaneTextField('about_title_3', {
                  label: "About_Title_3"
                }),
                PropertyPaneTextField('about_desc_3', {
                  label: "About_Desc_3"
                })
              ]
            },
          ]
        }
      ]
    };
  }
}

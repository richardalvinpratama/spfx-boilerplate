import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import { SPHttpClient, SPHttpClientResponse } from "@microsoft/sp-http";
import styles from './AnnouncementListWebPart.module.scss';
import { Lists, List } from "../../interfaces/ListInterface";
import { cmsAPI_URL } from "../../helpers/UrlHelper";

export interface IAnnouncementListWebPartProps {
  description: string;
}

export default class AnnouncementListWebPart extends BaseClientSideWebPart<IAnnouncementListWebPartProps> {
  private Data: List[];
  private Search: string = "";
  private OrderColumn: string = "Created";
  private Order: string = "desc";
  private startDate: Date;
  private endDate: Date;

  public async render(): Promise<void> {
    await this._renderListAsync();

    const SearchSortBox = `
      <div class="${styles.SearchSort}">
        <div class="${styles.Search}">
          <input type="text" id="search" name="search" placeholder="search by title">
        </div>
        <div class="${styles.DateFilter}">
          <input type="date" id="startDate" name="startDate" required>
          <input type="date" id="endDate" name="endDate" required>
          <button id="filterDate">Filter Date</button>
        </div>
      </div>
    `;

    this.domElement.innerHTML = `
      <section class="${styles.List}">
        ${SearchSortBox}
        <div class="${styles.Data}" id="listData">
        </div>
      </section>
    `;

    this.render_data();
    this.searchSort_script();
  }

  protected onInit(): Promise<void> {
    return super.onInit();
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  private searchSort_script() : void{
    const searchElement = document.getElementById("search") as HTMLInputElement;


    if(searchElement) {
      searchElement.addEventListener("input", async () => {
        this.Search = searchElement.value;
        await this._renderListAsync();
				this.render_data();
      });
    }

    document.getElementById("filterDate")?.addEventListener("click", async () => {
      const startDateElement = document.getElementById("startDate") as HTMLInputElement;
      const endDateElement = document.getElementById("endDate") as HTMLInputElement;

      this.startDate = new Date(startDateElement.value);
      this.endDate = new Date(endDateElement.value);

      await this._renderListAsync();
      this.render_data();
    })
  }

  private async _getListData(): Promise<Lists> {
    // Search Sort Filter Query Parameter
    const filterQueryArray: string[] = [];

    if (this.startDate instanceof Date) {
      const formattedStartDate = `${this.startDate.getFullYear()}-${
        this.startDate.getMonth() + 1
      }-${this.startDate.getDate()}`;
      const formattedEndDate = `${this.endDate.getFullYear()}-${
        this.endDate.getMonth() + 1
      }-${this.endDate.getDate()}`;
      filterQueryArray.push(
        `(Created ge '${formattedStartDate}T00:00:00Z' and Created le datetime'${formattedEndDate}T23:59:59Z')`
      );
    }

    if(this.Search !== ""){
      filterQueryArray.push(`substringof('${this.Search}', Title)`);
    }
    const filterQuery: string = "&$filter=" + filterQueryArray.join(" and ");

    return this.context.spHttpClient
			.get(
				`${cmsAPI_URL}/GetByTitle('Announcement')/Items?${filterQuery}&$orderby=${this.OrderColumn} ${this.Order}`,
				SPHttpClient.configurations.v1
			)
			.then((response: SPHttpClientResponse) => {
				return response.json();
			});
  }

  private async _renderListAsync(): Promise<void> {
    try {
      const response = await this._getListData();
      this.Data = response.value;
    } catch (error) {
      alert('Failed Get Announcement Data');
    }
  }
  
  private render_data(): void {
    const announcementCard = this.Data.map((data,index) => {
      let image;
			const thumbnailByLang = data.ThumbnailImage;

			if (thumbnailByLang) {
				image = JSON.parse(thumbnailByLang);
				image = window.location.origin + image.serverRelativeUrl;
			}
      
      return `
        <div class="${styles.Card}">
          <div>
            <img src="${image}" alt="${data.Title}" />
          </div>
          <div>
            <h2>${data.Title}</h2>
            <p>${data.Created}</p>
          </div>
          <a href="${data.Url_Map}" class="${styles.btn}">Show More</a>
        </div>
      `
    })

    console.log(announcementCard.join(""));

    const ListHTML = this.domElement.querySelector("#listData");
    if (ListHTML !== null) {
      ListHTML.innerHTML = announcementCard.join("");
    }

  }
}

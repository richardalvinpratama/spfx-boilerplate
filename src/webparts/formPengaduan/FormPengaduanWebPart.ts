import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { cmsAPI_URL } from '../../helpers/UrlHelper';
import {
  SPHttpClient,
  SPHttpClientResponse,
  // ISPHttpClientOptions  
} from '@microsoft/sp-http';

import styles from './FormPengaduanWebPart.module.scss';

export interface IFormPengaduanWebPartProps {
  description: string;
}

export default class FormPengaduanWebPart extends BaseClientSideWebPart<IFormPengaduanWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
    <div class="${styles.form_pengajuan}">
      <div class="${styles.box}">
        <img src="${require("../../assets/img/form-pengajuan.png")}">
      </div>
      <div class="${styles.box}">
        <div class="${styles.title}">
          <h1>Form Pengaduan</h1>
          <p>Jika terdapat masalah atau kendala dapat mengisi
          form pengaduan di bawah ini.</p>
        </div>
        <div class=${styles.form}>
          <label for="subject">Subject</label>
          <input type="text" id="subject" name="subject">
          <label for="message">Message</label>
          <input type="text" id="message" name="message">
          <button type="submit" id="btn-submit">Submit</button>
        </div>
      </div>
    </div>
    `;

    this._bindSave();
  }

  private _bindSave(): void{
    this.domElement.querySelector('#btn-submit')?.addEventListener('click', () => { this.addPengaduan();})
  }

  private addPengaduan(): void {
    const body: string = JSON.stringify({
      'Subject': (document.getElementById("subject") as HTMLInputElement)?.value,
      'Message': (document.getElementById("message") as HTMLInputElement)?.value,
    });

    this.context.spHttpClient.post(`${cmsAPI_URL}/getbytitle('Pengaduan')/items`,
      SPHttpClient.configurations.v1, {
      headers: {
        'Accept': 'application/json;odata=nometadata',
        'Content-type': 'application/json;odata=nometadata',
      },
      body: body
    })
      .then((response: SPHttpClientResponse) => {
        console.log(response);
        if (response.ok) {
          alert('Created Success');
        } else {
          alert('Created Failed');
        }
      }).catch(error => {
        console.log(error);
      });
  }

  protected onInit(): Promise<void> {
    return super.onInit();
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
}

import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import styles from './BannerWebPart.module.scss';

export interface IBannerWebPartProps {
  description: string;
}

export default class BannerWebPart extends BaseClientSideWebPart<IBannerWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
    <div class="${styles.banner}">
      <div class="${styles.bannerLeft}">
        <h1><span class="${styles.colorText}">Make</span> Everything<br>Digitally <span class="${styles.colorText}">Possible</span></h1>
        <p>Suitmedia is a <b>full-service digital agency</b> that helps brands in digital
        transformation through strategy, product developmen, and creative communication</p>
      </div>
      <div class="${styles.bannerRight}">
        <img src="${require("../../assets/img/banner.png")}" alt="Suitmedia Banner">
      </div>
    </div>
    `;
  }

  protected onInit(): Promise<void> {
    return super.onInit();
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
}

import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import { SPHttpClient, SPHttpClientResponse } from "@microsoft/sp-http";
import styles from './DetailTemplateWebPart.module.scss';
import { cmsAPI_URL, frontend_URL } from '../../helpers/UrlHelper';
import { Detail, Details } from '../../interfaces/DetailInterface';

export interface IDetailTemplateWebPartProps {
  description: string;
}

export default class DetailTemplateWebPart extends BaseClientSideWebPart<IDetailTemplateWebPartProps> {
	private Detail: Detail;
  private List: string | null;
	private ID: string | null;
  private isDataFound: boolean;

  public async render(): Promise<void> {
    await this._renderDetailAsync();

    this.domElement.innerHTML = `
    <section class="${styles.Detail}" id="detail">
    </section>
    `;

    this.render_data();
  }

  protected onInit(): Promise<void> {
    return super.onInit();
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  private async _getDetailData(): Promise<Details | null> {
    const url = new URL(window.location.href);
		const params = new URLSearchParams(url.search);
		this.List = params.get("ListName") === null ? null : params.get("ListName");
		this.ID = params.get("Slug") === null ? null : params.get("Slug");

    return this.context.spHttpClient
    .get(
      `${cmsAPI_URL}/GetByTitle('${this.List}')/Items?$filter=ID eq '${this.ID}'&$select=Title,Image,Body,Created,Author/Title&$expand=Author/Id`,
      SPHttpClient.configurations.v1
    )
    .then((response: SPHttpClientResponse) => {
      return response.json();
    });
  }

  private async _renderDetailAsync(): Promise<void> {
    try {
      const response = await this._getDetailData();
      if (response !== null && response.value.length > 0) {
				this.isDataFound = true;
				this.Detail = response.value[0];
			} else {
				this.isDataFound = false;
			}
    } catch (error) {
      alert('Failed Get Detail Data');
    }
  }

  private render_data(): void {
    const header = `
      <div class="${styles.DetailHeader}">
        <a href="${frontend_URL}"><b>Back To Home</b></a>
      </div>
    `;

    let body = `
      <div>
        <h1>Data tidak ditemukan</h1>
      </div>
    `;

    if(this.isDataFound){
      let image;
      if (this.Detail.Image) {
        image = JSON.parse(this.Detail.Image);
        image = window.location.origin + image.serverRelativeUrl;
      }

      body = `
        <div>
          <img src="${image}" alt="${this.Detail.Title}" style="width:100%">
        </div>
        <div class="${styles.BodyContent}">
          <div class="${styles.ContentTitle}">
            <div>
              <b>${this.Detail.Title}</b>
            </div>
            <div class="${styles.author}">
              ${this.Detail.Author.Title}
            </div>
          </div>
          <div class="${styles.ContentDetail}">
            ${this.Detail.Body}
          </div>
        </div>
      `;
    }

    const Detail = `
      ${header}
      <div class="${styles.DetailBody}">
        ${body}
      </div>
    `;

    const DetailElement = this.domElement.querySelector("#detail");
    if (DetailElement !== null) {
      DetailElement.innerHTML = Detail;
    }
  }
}

export const base_URL = "https://suitmedia732.sharepoint.com";
export const cms_URL = `${base_URL}/sites/SuitmediaCMS`;
export const frontend_URL = `${base_URL}/sites/Suitmedia`;

export const cmsAPI_URL = `${cms_URL}/_api/web/lists`;
export const frontendAPI_URL = `${frontend_URL}/_api/web/lists`;

export default class UrlHelper {

}